using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interaction_2 : MonoBehaviour
{
    public float distance = 2f;
    
    public static Interaction_2 Instance { get; private set; }
    
    private void Awake()
    {
        Instance = this;
    }

    // Update is called once per frame
    void Update()
    {
        //If the interaction menu is not active
        if (InteractionMenu.Instance.gameObject.activeSelf == false)
        {
            if (Input.GetButtonDown("Interact") == true) //and check that no other menus are open
            {
                //if ray cast hits a collider
                if (Physics.SphereCast(transform.position, 1, transform.forward, out RaycastHit hit, distance))
                {
                    //Check if collider has interaction component
                    if (hit.collider.TryGetComponent(out IInteraction_2 interaction) == true)
                    {
                        //activate the interaction
                        InteractionMenu.Instance.Activate(interaction);
                        //draw green debug line
                        Debug.DrawRay(transform.position, transform.forward * distance, Color.green, 0.1f);
                    }
                    else
                    {
                        //else draw yellow debug line
                        Debug.DrawRay(transform.position, transform.forward * distance, Color.yellow, 0.1f);
                    }
                }
                else
                {
                    //else draw red debug line
                    Debug.DrawRay(transform.position, transform.forward * distance, Color.red, 0.1f);
                }
            }
        }
        else
        {
            //if interact button is released
            if (Input.GetButtonUp("Interact") == true)
            {
                //deactivate interaction menu
                InteractionMenu.Instance.Deactivate();

            }
        }
    }
}

public interface IInteraction_2
{
    void Inspect();
}

public interface IFauna : IInteraction_2
{
    void Photograph(string folderPath = null);
}

public interface IKey : IInteraction_2
{
    void Pickup();
}

public interface IKeyLock : IInteraction_2
{
    bool Unlock();
}