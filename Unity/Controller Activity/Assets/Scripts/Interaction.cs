﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interaction : MonoBehaviour
{
    public float distance = 2f;

    // Update is called once per frame
    void Update()
    {
        if(Input.GetButtonDown("Fire1") == true)
        {
            Ray ray = new Ray(transform.position, transform.forward);
            if(Physics.Raycast(ray, out RaycastHit rayHit, distance) == true)
            {
                if(rayHit.transform.TryGetComponent(out IInteraction interaction) ==true)
                {
                    Debug.DrawRay(ray.origin, ray.direction * distance, Color.green, 1f);
                    interaction.Activate();
                }
                else
                {
                    Debug.DrawRay(ray.origin, ray.direction * distance, Color.yellow, 1f);

                }
            }
            else
                Debug.DrawRay(ray.origin, ray.direction * distance, Color.red, 1f);
        }

    }
}
public interface IInteraction
{
    void Activate();
}