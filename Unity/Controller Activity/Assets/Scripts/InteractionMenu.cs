using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine.UI;
using UnityEngine;

public class InteractionMenu : MonoBehaviour
{
    public Image optionA, optionB;
    public Text optionBText;

    public static InteractionMenu Instance { get; private set; }

    private IInteraction_2 currrentInteraction;
    private int selectedOptionIndex = -1;
    private string dataPath;

    private void Awake()
    {
        dataPath = Application.dataPath + "/Resources/PhotoGallery";
        Instance = this;
        if(Directory.Exists(dataPath) == false)
        {
            Directory.CreateDirectory(dataPath);
        }
    }

    void Start()
    {
        Deactivate();
    }

    void Update()
    {
        //mouse scroll wheel functionality
        if(gameObject.activeSelf == true)
        {
            if(Input.GetAxis("Mouse ScrollWheel") != 0)
            {
                selectedOptionIndex += Mathf.CeilToInt(Input.mouseScrollDelta.y);
                if(selectedOptionIndex < 0)
                {
                    selectedOptionIndex = 1;
                }
                else if(selectedOptionIndex > 1)
                {
                    selectedOptionIndex = 0;
                }
                updateSelection();
            }
        }
    }

    void updateSelection()
    {
        switch (selectedOptionIndex)
        {
            case 0:
                optionA.color = Color.white;
                optionB.color = Color.grey;
                break;
            case 1:
                optionA.color = Color.grey;
                optionB.color = Color.white;
                break;
        }
    }

    public void Activate(IInteraction_2 interaction)
    {
        currrentInteraction = interaction;
        if (GetInteractionAsType(out IFauna fauna) == true)
        {
            optionBText.text = "Photograph";
            optionBText.color = Color.black;
        }
        else if(GetInteractionAsType(out IKey key) == true)
        {
            optionBText.text = "Pick Up";
            optionBText.color = Color.black;
        }
        else if (GetInteractionAsType(out IKeyLock keyLock) == true)
        {
            //Figure out if the lock is unlocked
            //else if player has the key
            //else if the player does not have the key
        }
        selectedOptionIndex = 0;
        updateSelection();
        gameObject.SetActive(true);
    }

    public void Deactivate()
    {
        //Activate current selected index 
        //set menu to inactive
        switch (selectedOptionIndex)
        {
            case 0:
                currrentInteraction.Inspect();
                break;
            case 1:
                if (GetInteractionAsType(out IFauna fauna) == true)
                {
                    Photograph(fauna);
                }
                else if (GetInteractionAsType(out IKey key) == true)
                {
                    key.Pickup();
                }
                else if (GetInteractionAsType(out IKeyLock keyLock) == true)
                {
                    keyLock.Unlock();
                }
                break;
        }
        gameObject.SetActive(false);
        selectedOptionIndex = -1;
        currrentInteraction = null;
    }

    private void Photograph(IFauna fauna)
    {
        DirectoryInfo dirInfo = new DirectoryInfo(dataPath);
        FileInfo[] info = dirInfo.GetFiles("*.png");
        int count = 0;
        foreach(FileInfo i in info)
        {
            if(i.Name.Contains("UWW_") == false)
            {
                count++;

            }
        }

        if (count < 30)
        {
            fauna.Photograph(dataPath);
        }
        else
        {
            fauna.Photograph();
        }
    }

    //Returns the current interaction as the specified interaction type
    private bool GetInteractionAsType<InteractionType>(out InteractionType target) where InteractionType : IInteraction_2
    {
        if(currrentInteraction is InteractionType interaction)
        {
            target = interaction;
            return true;
        }
        target = default;
        return false;
    }
}
