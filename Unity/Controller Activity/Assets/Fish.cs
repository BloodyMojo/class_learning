using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fish : MonoBehaviour, IFauna
{
    public void Inspect()
    {
        Debug.Log("Inspecting" + name);
    }

    public void Photograph(string folderPath = null)
    {
        if(folderPath != null)
        {
            Debug.Log("Take screenshot and savew to " + folderPath);
            string date = System.DateTime.Now.TimeOfDay.ToString().Replace(":", "-");
            ScreenCapture.CaptureScreenshot(folderPath + $"/UWW_{date}.png", 2);
        }
    }
}
