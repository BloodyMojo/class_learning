#Example of writing text to a text file
text = "Line 1\nLine 2\nLine 3\nLine 4\nLine 5"
file = open("text.txt", "w")
file.write(text)
file.close

#Example of reading text from a text file
file = open("text.txt", "r")
for line in file:
    print(line)
file.close

#Method for writing content to a file 
def write_to_file(name, content):
    file = open(name + ".txt", "w")
    file.write(content)
    file.close

def read_file(name):
    file = open(name + ".txt", "r")
    for line in file:
        print(line)
    file.close()

#Method for searching the file for target content 
def linear_file_search(name, target):
    file = open(name + ".txt", "r")
    for line in file:
        if(line.find (str(target)) is not -1):
            file.close()
            return target + " has been found."
    file.close()
    return target + "was not found in the file."

write_to_file("test", "Line 1\nLine 2\nLine 3")
read_file("test")
write_to_file("new file", "Some other boring stuff. /nwoooooooooooo")
read_file("new file")
result = linear_file_search("test", "Line 3")
print(result)